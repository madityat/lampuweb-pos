<?php
    function sendResponse($data, $message, $code) {
        header('Content-Type: application/json');
        http_response_code($code);
        echo json_encode(array(
            'success' => ($code == 200) ? true : false,
            'code' => $code,
            'data' => ($data) ? $data : null, 
            'message' => $message
        ));
        exit(0);
    }
    function getHeaderAuthorization() {
        $headers = apache_request_headers();

        foreach ($headers as $header => $value) {
            if ($header == 'Authorization') {
                return $value;
            }
        }
    }
    function isAuthenticated($authorization) {
        $auth = 'admin:admin';
        if (strpos($authorization, 'Basic') === false) {
            return [
                'success' => false,
                'message' => 'Authorization must be basic',
                'code' => 400
            ];
        }
        $authorization = explode(' ', $authorization);
        return [
            'success' => (base64_encode($auth) == $authorization[1]) ? true : false,
            'message' => (base64_encode($auth) == $authorization[1]) ? 'Success' : 'Unauthorized',
            'code' => (base64_encode($auth) == $authorization[1]) ? 200 : 401
        ];
    }

    // check if authorization has been added to header
    if(!getHeaderAuthorization()) {
        sendResponse(null, 'Authorization header needed', 400);
    }

    // check if authorization is authenticated
    $isAuthenticated = isAuthenticated(getHeaderAuthorization());
    if (!$isAuthenticated['success']) {
        sendResponse(null, $isAuthenticated['message'], $isAuthenticated['code']);
    }

    // get the HTTP method, path and body of the request
    $inputJSON = file_get_contents('php://input');
    $input = json_decode($inputJSON, TRUE); //convert JSON into array
    if (!$input || count(@$input) <= 0) {
        sendResponse(null, 'No value given', 400);
    }

    if ($_SERVER['SERVER_NAME'] == 'localhost') {
        $link = mysqli_connect('localhost', 'root', '', 'db_pos');
    } else {
        $link = mysqli_connect('localhost', 'appstud1_root', 'Appstudio123', 'appstud1_pos');
    }
    if ($link->connect_errno) {
        echo "Failed to connect to MySQL: " . $link->connect_error;
        exit();
    }
    mysqli_set_charset($link,'utf8');
    
    $index = 0;
    $values = "";
    foreach($input as $transaksi) {
        $values .= "('{$transaksi['merchant']}','{$transaksi['username']}','{$transaksi['no_faktur']}','{$transaksi['tanggal']}','{$transaksi['bayar']}', '{$transaksi['total']}', 1)";
        if ($index < count($input)-1) {
            $values .= ', ';
        }
        $index++;
    } 
    $sql = "INSERT INTO tb_transaksi (merchant, username, no_faktur, tanggal, bayar, total, backup) VALUES $values";

    $result = mysqli_query($link, $sql);
    if (!$result) {
        $msg = mysqli_error($link).": $sql";
        sendResponse(null, $msg, 500);
    }
    foreach($input as $transaksi) {
        $index = 0;
        $values = "";
        foreach($transaksi['detail'] as $detail) {
            $values .= "('{$detail['no_faktur']}', '{$detail['kode_produk']}', '{$detail['nama_produk']}', '{$detail['quantity']}', '{$detail['subtotal']}')";
            if ($index < count($transaksi['detail'])-1) {
                $values .= ', ';
            }
            $index++;
            mysqli_query($link, "INSERT INTO tb_detail_transaksi (no_faktur, kode_produk, nama_produk, quantity, subtotal) VALUES $values");
        }
    }
    
    $msg = "Data has been added";    
    sendResponse($input, $msg, 200);

    // close mysql connection
    mysqli_close($link);
?>