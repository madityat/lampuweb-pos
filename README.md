## Simple Guide

1. Import database `db_pos.sql`
2. Change database config on 
    - `api/backupTransaksi.php`
    - `api/cronBackupTransaksi.php`
    - `models/App.php`
3. Change Backup API Endpoint on
    - `api/cronBackupTransaksi.php`
    - `index.php`
4. POSTMAN Collection
    - https://www.getpostman.com/collections/1dff275fb3bb50c32bee